@extends('layout.main')

@section('container')
<section class="page-section" id="services">
            <div class="container">
                <div class="text-center"><br>
                    <h2 class="section-heading text-uppercase">Sekilas</h2>
                    <h3 class="section-subheading text-muted">Sekilas tentang saya.</h3>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-user-tie fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">Status</h4>
                        <p class="text-muted">Status saat ini yaitu sebagai mahasiswa aktif Perguruan Tinggi Negeri (PTN)</p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-laptop fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">Pendidikan</h4>
                        <p class="text-muted">Sedang menempuh pendidikan S1 bidang studi Teknik Informatika di Universitas Pendidikan Ganesha</p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-home fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">Alamat</h4>
                        <p class="text-muted">Bertempat tinggal di Banjar Dinas Darma Winangun, Desa Tianyar, Kecamatan Kubu, Kabupaten Karangasem, Bali</p>
                    </div>
                </div>
            </div>
        </section>
    @include('partials.footer')
@endsection
