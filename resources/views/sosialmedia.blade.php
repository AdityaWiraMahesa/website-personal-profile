@extends('layout.main')

@section('container')
<section class="page-section bg-light" id="team">
            <div class="container">
                <div class="text-center"><br>
                    <h2 class="section-heading text-uppercase">Sosial Media</h2>
                    <br>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="team-member">
                            <img class="mx-auto rounded-circle" src="assets/img/team/1.jpg" alt="..." />
                            <h4>Adithya</h4>
                            <p class="text-muted">Facebook</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="team-member">
                            <img class="mx-auto rounded-circle" src="assets/img/team/2.jpg" alt="..." />
                            <h4>adithyawm_</h4>
                            <p class="text-muted">Instagram</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="team-member">
                            <img class="mx-auto rounded-circle" src="assets/img/team/3.jpg" alt="..." />
                            <h4>adithyamahesa_</h4>
                            <p class="text-muted">Line</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center"><p class="large text-muted">Hubungi social media diatas jika ada hal yang ingin disampaikan ataupun sebagainya.</p></div>
                </div>
            </div>
        </section> 
    @include('partials.footer')   
@endsection