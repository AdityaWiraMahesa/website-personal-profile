@extends('layout.main')

@section('container')
<header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Selamat Datang di Galeri Photography!</div>
                <div class="masthead-heading text-uppercase">Aditya Wira Mahesa</div>
                <a class="btn btn-primary btn-xl text-uppercase" href="/sekilas">Jelajah</a>
            </div>
        </header> 
@endsection