@extends('layout.main')

@section('container')
<section class="page-section" id="about">
            <div class="container">
                <div class="text-center"><br>
                    <h2 class="section-heading text-uppercase">Tentang</h2>
                    <br>
                </div>
                <ul class="timeline">
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/1.jpg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2007-2013</h4>
                                <h4 class="subheading">SD Negeri 2 Tianyar</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">Pada tahun 2007-2013 menempuh pendidikan di SD Negeri 2 Tianyar, Kecamatan Kubu, Kabupaten Karangasem, Provinsi Bali</p></div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/2.jpg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2013-2016</h4>
                                <h4 class="subheading">SMP Negeri 2 Kubu</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">Pada tahun 2013-2016 menempuh pendidikan di SMP Negeri 2 Kubu, Kecamatan Kubu, Kabupaten Karangasem, Provinsi Bali</p></div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/3.jpg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2016-2019</h4>
                                <h4 class="subheading">SMA Negeri 1 Kubu</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">Pada tahun 2016-2019 menempuh pendidikan di SMA Negeri 1 Kubu, Kecamatan Kubu, Kabupaten Karangasem, Provinsi Bali. Dengan mengambil jurusan IPA.</p></div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/about/4.jpg" alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Agustus 2019-Now</h4>
                                <h4 class="subheading">Universitas Pendidikan Ganesha</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">Pada tahun 2019, melanjutkan pendidikan di Universitas Pendidikan Ganesha, dengan mengambil program studi Pendidikan Teknik Informatika, Jurusan Teknik Informatika, Fakultas Teknik dan Kejuruan</p></div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>
                                Kisah
                                <br />
                                Saya
                                <br />
                                Selanjutnya!
                            </h4>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    @include('partials.footer')    
@endsection