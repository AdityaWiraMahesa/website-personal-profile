<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SosialMediaController extends Controller
{
    public function index(){
        return view('SosialMedia', [
            "title" => "Sosial Media"
        ]);
    }
}
