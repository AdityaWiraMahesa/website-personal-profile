<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class,'index']);
Route::get('/sekilas', [App\Http\Controllers\SekilasController::class,'index']);
Route::get('/galeri', [App\Http\Controllers\GaleriController::class,'index']);
Route::get('/tentang', [App\Http\Controllers\TentangController::class,'index']);
Route::get('/sosialmedia', [App\Http\Controllers\SosialMediaController::class,'index']);
Route::get('/kontak', [App\Http\Controllers\KontakController::class,'index']);